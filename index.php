<?php include('idiomas/esp.php') ?>

<!DOCTYPE html>
<html lang="en">

<?php include('Recursos/head.php') ?>

    <body>

       <?php include('partes/nav.php') ?>
       <!-- Page Header -->
     <!-- Set your background image for
     this header on the line below. -->
     <header class="intro-header" style="background-image: url('img/home-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1><?php echo $texto_principal ?></h1>
                        <hr class="small">
                        <span class="subheading"><?php echo $texto_secundario ?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-preview">
                    <a href="post.html">
                        <h2 class="post-title">
                            <?php echo $parrafo_titulo ?>
                        </h2>
                        <h3 class="post-subtitle">
                         <?php echo $parrafo_subtitulo ?>
                     </h3>
                 </a>
                 <p class="post-meta"><?php echo $parrafo_autor ?></p>
             </div>
             <hr>
         </div>
     </div>
 </div>

 <hr>
 <?php include('partes/footer.php') ?>
<?php include('Recursos/script.php') ?>

</body>

</html>
