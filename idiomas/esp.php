<?php 
$titulo='Ideas de Café';
$texto_principal='Ideas de Café';
$texto_secundario='Un blog de programación y personalidad';
$parrafo_titulo='Puedes explorar las siguientes características';
$parrafo_subtitulo='Dejaras de ver problemas';
$parrafo_autor='Creado por Andrea, Octubre 23, 2016';
$boton_inicio='Inicio';
$boton_sobrenosotros='Sobre Nosotros';
$boton_contacto='Contacto';
$copy_right='Todos los derechos reservados';
$firma='Tu sitio web 2016';
$texto_principal2='Sobre Nosotros';
$texto_secundario2='Mejoramos tu servicio';
$parrafo_texto2='Entregamos el mejor servicio para tus negocios. Podrás confiarnos todas tus ideas para desarrollarlas de la mejor forma posible';
$autor='Andrea Cardona Rojas';
$texto_principal3='Contáctanos';
$texto_secundario3='¿Tienes preguntas? No dudes en contcatarnos';
$parrafo_texto3='¿Deseas ponerte en contacto? Deja tus datos en el siguiente formulario y trataremos de contactarte en menos de 24 horas';
$nombre='Nombre';
$correo='Correo Electrónico';
$telefono='Teléfono';
$mensaje='Mensaje';



 ?>