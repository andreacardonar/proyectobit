<?php include('idiomas/esp.php') ?>

<!DOCTYPE html>
<html lang="en">

<?php include('Recursos/head.php') ?>

<body>

    <?php include('partes/nav.php') ?>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('img/about-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1><?php echo $texto_principal2 ?></h1>
                        <hr class="small">
                        <span class="subheading"><?php echo $texto_secundario2 ?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p><?php echo $parrafo_texto2 ?></p>
            </div>
        </div>
    </div>

    <hr>

    <?php include('partes/footer.php') ?>

    <?php include('Recursos/script.php') ?>

</body>

</html>
